package main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


/**
 * @author Rodrigo Lafayette da Silva
 * email: rodrigolafayette@hotmail.com
 * BitBucket: RLafayette
 * Data: 23/11/2016 Revis�o: 17/03/2018
 */

public class FileProcessor {

	public static ArrayList<String> read(String caminho) throws IOException {
		ArrayList<String> linhas = new ArrayList<String>();

		FileReader reader;
		BufferedReader buffer;

		try {
			reader = new FileReader(caminho);
			buffer = new BufferedReader(reader);

			String linha = "";
			while (true) {
				linha = buffer.readLine();
				if (linha != null) {
					if (!linha.equals("") && !linha.equals("\n")) {
						linhas.add(linha);
						linha = null;
						System.gc();
					}
				} else
					break;
			}

			buffer.close();
			reader.close();
		} catch (FileNotFoundException f) {
			throw f;
		} catch (IOException i) {
			throw i;
		}

		return linhas;
	}

	/**
	 * Write in a file.
	 * 
	 * @param caminho
	 *            Directory where is the file in that will occur the writing.
	 * @param content
	 *            String that will be written in the file.
	 * @throws IOException
	 */
	public static void write(String caminho, String content) throws IOException {
		if (caminho == null || content == null)
			return;

		FileWriter writer = null;
		BufferedWriter buffer = null;

		try {
			writer = new FileWriter(caminho, true);
			buffer = new BufferedWriter(writer);
			buffer.append(content);
			buffer.newLine();

			buffer.close();
			writer.close();
		} catch (FileNotFoundException f) {
			throw f;
		} catch (IOException i) {
			throw i;
		}
	}

	public static void remove(String caminho, String newLine) throws IOException {

		if (newLine == null)
			return;

		ArrayList<String> fileContent = new ArrayList<String>();

		try {
			FileReader reader = new FileReader(caminho);
			BufferedReader buffer = new BufferedReader(reader);

			String line = "";
			while (true) {
				line = buffer.readLine();
				if (line != null) {
					if (!line.equals(newLine)) {
						fileContent.add(line);
						line = null;
						System.gc();
					}
				} else
					break;
			}

			buffer.close();
			reader.close();
		} catch (FileNotFoundException f) {
			throw f;
		} catch (IOException i) {
			throw i;
		}

		// Make the file empty
		try {
			FileWriter writer = new FileWriter(caminho);
			BufferedWriter buffer = new BufferedWriter(writer);
			writer.close();
			buffer.close();
		} catch (IOException i) {
			throw i;
		}

		// Write in the file
		for (String e : fileContent)
			write(caminho, e);
	}
}
